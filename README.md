# SiLA 2 Interoperability Testing
## Testing principles
### Separation of connection and communication tests
Tests are split in two main categories:
1. **Connection tests** - A test is successful if a SiLA Client can connect to a SiLA Server and communicate with it
2. **Communication tests** - A test starts with an established connection and tests the content of communication (request message, request metadata, response message, response status)

### Source of truth
This repository will provide testing tools which can be used to check if a given SiLA 2 implementation satisfies various parts of the SiLA 2 specification.
The SiLA 2 specification is the source of truth for all these tests.

We do our best to keep the tools as aligned with the specification as possible.
When you suspect there is a flaw in one of the tools, please report that as soon as possible.

### Connection tests
These tests deal with how SiLA Clients connect to SiLA Servers:

- Encrypted and unencrypted communication
- Direct connection and SiLA Server Discovery
- Client- and server-initiated connection

### Communication tests
These tests deal with the gRPC-based communication between SiLA Clients and SiLA Servers:

- Request messages
- Response messages
- Request metadata
- Response error statuses

Here again, both client- and server-initiated connection methods shall be tested.

### Reporting
Every implementation shall execute the provided testing tools at their own discretion, e.g. during the release process.
These tools generate reports, of which the latest version has to be made available publicly, e.g. as CI artifact.
This repository shall collect and aggregate all reports to generate a unified report on the interoperability of the existing SiLA 2 implementations.

## Provided tools
NOTE: These tools are only first drafts and test a minimal subset of the specification.

### Communication testing
The tool and documentation for communication testing can be found [here](communication-tests).
