import os
import json
import urllib.request
import urllib.error
import shutil

report_sources_file = "report-sources.json"
report_sources = json.load(open(report_sources_file))

os.makedirs("report-webpage/report-xmls", exist_ok=True)
os.makedirs("report-webpage/report-xmls/client-tests", exist_ok=True)
os.makedirs("report-webpage/report-xmls/server-tests", exist_ok=True)

for name, url in report_sources["test-client-reports"].items():
    try:
        urllib.request.urlretrieve(url, "report-webpage/report-xmls/server-tests/" + name + ".xml")
    except urllib.error.HTTPError:
        print(f"Couldn't find resource: {url}")

for name, url in report_sources["test-server-reports"].items():
    try:
        urllib.request.urlretrieve(url, "report-webpage/report-xmls/client-tests/" + name + ".xml")
    except urllib.error.HTTPError:
        print(f"Couldn't find resource: {url}")

shutil.copyfile(report_sources_file, "report-webpage/report-sources.json")
