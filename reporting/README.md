# Reporting tools
This is where all tools related to reporting shall be placed:
- download test reports from repos (see [report-sources.json](./report-sources.json))
- aggregate test reports
- generate static website to display aggregated report

The current version can be found [here](https://sila2.gitlab.io/sila_interoperability/).
