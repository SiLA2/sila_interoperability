document.addEventListener("DOMContentLoaded", async function() {
    console.time("start until end");
    createTableWithData();
});


/**
 * Main-Function, which gets the JSON-object with all the information and which creates the table with these information
 */
async function createTableWithData() {
    let xmlFilePathsOfAllReports = await createObjectWithReportXMLFilePaths();
    let JSONObjects = await storeXMLFilesInOneJSONObject(xmlFilePathsOfAllReports);
    
    console.log(JSONObjects);

    let clientReportsTable = createTable(JSONObjects["clientReports"]);
    document.getElementById("client-side-report-table").append(clientReportsTable);

    let serverReportsTable = createTable(JSONObjects["serverReports"]);
    document.getElementById("server-side-report-table").append(serverReportsTable);

    console.timeEnd("start until end");
}


/**
 * Stores all the file-paths of the XML-files in one object
 * with one array for client-reports and one array for server-reports
 * @returns {Object} xmlFilePathsOfAllReports
 */
async function createObjectWithReportXMLFilePaths() {
    const reportSourcesObject = await getReportSources();
    
    //** Get the client- and server-test report-names (from report-sources.json), add them to the updated file path of every test-report and push them into an array: */
    let xmlFilesOfClientReports = pushFilePathOfTestReportsToArray(reportSourcesObject["test-client-reports"], "report-xmls/server-tests/");
    let xmlFilesOfServerReports = pushFilePathOfTestReportsToArray(reportSourcesObject["test-server-reports"], "report-xmls/client-tests/");

    let xmlFilePathsOfAllReports = { "clientReports": xmlFilesOfClientReports, "serverReports": xmlFilesOfServerReports };

    return xmlFilePathsOfAllReports;
}


/**
 * Get every source/link of each testsuite and store them into one object for each report
 * with name of testsuite as key and URL for download of XML-file as value
 * @returns {Object} reportSourcesObject
 */
async function getReportSources() {
    const response = await fetch('report-sources.json');
    const reportSourcesObject = await response.json();

    return reportSourcesObject;
}


/**
 * Adds the testsuite-names (from report-source.json) to the updated file path of every test-report
 * @param {Object} testReportFiles - Object with source/link of each testsuite (key = name, value = URL)
 * @param {String} testReportDirectory - Path of folder for XML-files
 * @returns {Array} xmlFilesOfReports
 */
function pushFilePathOfTestReportsToArray(testReportFiles, testReportDirectory) {
    let xmlFilesOfReports = [];

    for (const testreportName in Object.keys(testReportFiles)) {
        xmlFilesOfReports.push(testReportDirectory + encodeURIComponent(Object.keys(testReportFiles)[testreportName]) + ".xml");
    }

    return xmlFilesOfReports;
}

/**
 * Stores all the necessary data into the JSON-object
 * @param {Object} xmlFilePathsOfAllReports - Object with two arrays (client-reports and server-reports)
 * @returns {Object} JSONObject
 */
async function storeXMLFilesInOneJSONObject(xmlFilePathsOfAllReports) {
    const JSONObjectsOfClientReports = await createJSONObjectsOfXMLFileForArray(xmlFilePathsOfAllReports, "clientReports");
    const JSONObjectsOfServerReports = await createJSONObjectsOfXMLFileForArray(xmlFilePathsOfAllReports, "serverReports");
    
    let JSONObjects = { "clientReports": JSONObjectsOfClientReports, "serverReports": JSONObjectsOfServerReports }

    return JSONObjects;
}


async function createJSONObjectsOfXMLFileForArray(xmlFilePathsOfAllReports, reportType) {
    let JSONObjectsOfReports = [];

    for (const indexOfLink in xmlFilePathsOfAllReports[reportType]) {
        try {
            let xmlDocumentObjectOfReport = await getXMLDocumentObjectOfReport(xmlFilePathsOfAllReports[reportType][indexOfLink]);
            let JSONObject = createJSONObjectOfXMLDocument(xmlDocumentObjectOfReport);
            JSONObjectsOfReports.push(JSONObject);
        } catch(error) {
            console.error(error);  // file not found: resource not available
            continue;
        }
    }

    return JSONObjectsOfReports;
}


async function getXMLDocumentObjectOfReport(filePathOfReport) {
    let response = await fetch(filePathOfReport);
    if (!response.ok) {
        throw "Not found: "+ filePathOfReport;
    }
    let xmlAsString = await response.text();
    let xmlDocumentObjectOfReport = parseXmlString(xmlAsString);

    return xmlDocumentObjectOfReport;
}


function parseXmlString(xmlString) {
    return new window.DOMParser().parseFromString(xmlString, "text/xml");
}


function createJSONObjectOfXMLDocument(xmlDocumentObjectOfReport) {
    const JSONObject = {
        "title": extractTestsuiteName(xmlDocumentObjectOfReport),
        "version": extractVersionNumber(xmlDocumentObjectOfReport),
        "tests": {
        }
    };

    // iterate over all testcase nodes, extract names, store name and node in "tests" field
    let testcaseNodeIterator = extractTestcaseNodes(xmlDocumentObjectOfReport);
    const numTestcases = xmlDocumentObjectOfReport.evaluate("count(//testsuites/testsuite/testcase)", xmlDocumentObjectOfReport).numberValue;
    for (let i = 0; i < numTestcases; i++) {
        const testcaseNode = testcaseNodeIterator.iterateNext();
        const className = extractClassName(xmlDocumentObjectOfReport, testcaseNode);
        if (!Object.keys(JSONObject["tests"]).includes(className)) {
            JSONObject["tests"][className] = {};
        }

        const testcaseName = extractTestcaseName(xmlDocumentObjectOfReport, testcaseNode);
        JSONObject["tests"][className][testcaseName] = testcaseNode;
    }

    return JSONObject
}


/**
 * 
 * @param {XMLDocument} xmlDocument 
 * @returns {String}
 */
function extractTestsuiteName(xmlDocument) {
    return xmlDocument.evaluate("string(//testsuites/testsuite/@name)", xmlDocument).stringValue;
}


/**
 * 
 * @param {XMLDocument} xmlDocument 
 * @returns {String}
 */
function extractVersionNumber(xmlDocument) {
    let version = xmlDocument.evaluate("string(//testsuites/testsuite/properties/property/@value)", xmlDocument).stringValue;
    if (version == "") {
        return null
    }
    return version;
}


/**
 * 
 * @param {XMLDocument} xmlDocument
 * @returns {XPathResult}
 */
function extractTestcaseNodes(xmlDocument) {
    return xmlDocument.evaluate("//testsuites/testsuite/testcase", xmlDocument);
}


/**
 * 
 * @param {XMLDocument} xmlDocument 
 * @param {Node} testcaseNode 
 * @returns {String}
 */
function extractTestcaseName(xmlDocument, testcaseNode) {
    return xmlDocument.evaluate("string(./@name)", testcaseNode).stringValue;
}


/**
 * 
 * @param {XMLDocument} xmlDocument 
 * @param {Node} testcaseNode 
 * @returns {String}
 */
function extractClassName(xmlDocument, testcaseNode) {
    return xmlDocument.evaluate("string(./@classname)", testcaseNode).stringValue;
}


function getReportWithHighestVersionNumber(JSONObjects) {
    let testreportWithHighestVersionNumber;
    let highestVersionNumberAsArray = [0, 0, 0];

    for (const indexOfTestreport in JSONObjects) {
        if (!(JSONObjects[indexOfTestreport].version == null)) {
            let currentVersionNumberAsArray = JSONObjects[indexOfTestreport].version.split('.').map((x) => parseInt(x));
            
            if (currentVersionNumberAsArray[0] > highestVersionNumberAsArray[0]) {
                highestVersionNumberAsArray = currentVersionNumberAsArray;
                testreportWithHighestVersionNumber = JSONObjects[indexOfTestreport];
            } else if ((currentVersionNumberAsArray[0] == highestVersionNumberAsArray[0]) && (currentVersionNumberAsArray[1] > highestVersionNumberAsArray[1])) {
                highestVersionNumberAsArray = currentVersionNumberAsArray;
                testreportWithHighestVersionNumber = JSONObjects[indexOfTestreport];
            } else if ((currentVersionNumberAsArray[0] == highestVersionNumberAsArray[0]) && (currentVersionNumberAsArray[1] == highestVersionNumberAsArray[1]) && (currentVersionNumberAsArray[2] > highestVersionNumberAsArray[2])) {
                highestVersionNumberAsArray = currentVersionNumberAsArray;
                testreportWithHighestVersionNumber = JSONObjects[indexOfTestreport];
            }
        }
    }

    return testreportWithHighestVersionNumber;
}




function createTable(JSONObject) {
    // console.log(JSONObject);
    let table = document.createElement("table");
    table.setAttribute("class", "table");
    table.append(setColgroup(JSONObject));
    table.append(createTableHead(JSONObject));
    table.append(createTableBody(JSONObject));

    return table;
}


function setColgroup(JSONObject) {
    let colgroup = document.createElement("colgroup");

    for (const indexOfTestreport in JSONObject) {
        const col = document.createElement("col");
        col.setAttribute("style", "");
        colgroup.appendChild(col);
    }

    setColumnForNameOfTestcase(colgroup);

    return colgroup;
}


function setColumnForNameOfTestcase(colgroup) {
    const columnForNameOfTestcase = document.createElement("col");
    columnForNameOfTestcase.setAttribute("style", "width: 40%");
    colgroup.prepend(columnForNameOfTestcase);
}


function createTableHead(JSONObject) {
    const thead = document.createElement("thead");

    const tr = document.createElement("tr");
    thead.appendChild(tr);

    const th1 = document.createElement("th");
    th1.textContent = "Testcases";
    tr.appendChild(th1);

    for (const indexOfTestreport in JSONObject) {
        const tableHeadElement = document.createElement("th");
        const tableHeadTitleOfReport = JSONObject[indexOfTestreport].title;
        const tableHeadTitleOfReportText = document.createTextNode(tableHeadTitleOfReport);
        tableHeadElement.appendChild(tableHeadTitleOfReportText);
        tr.appendChild(tableHeadElement);
    }

    return thead;
}


function createTableBody(JSONObject) {
    const tbody = document.createElement("tbody");
    const reportWithHighestVersionNumber = getReportWithHighestVersionNumber(JSONObject);

    for (const referenceTestClassName of Object.keys(reportWithHighestVersionNumber.tests)) {
        const referenceTestNames = Object.keys(reportWithHighestVersionNumber.tests[referenceTestClassName]);
        
        const testClassResults = extractTestClassResults(JSONObject, referenceTestClassName, referenceTestNames);

        const trElements = createTableBlock(referenceTestClassName, referenceTestNames, testClassResults);
        for (const trElement of trElements) {
            tbody.appendChild(trElement);
        }
    }

    return tbody;
}


function createTableBlock(referenceTestClassName, referenceTestNames, testClassResults) {
    const bootstrapTargetClass = "collapseTestclass-" + createUniqueId();
    const testclassRow = createTestclassRow(referenceTestClassName, testClassResults, bootstrapTargetClass);
    const trElements = [testclassRow];

    for (const testName of Object.values(referenceTestNames)) {
        const testRow = createTestRow(testName, testClassResults[testName], bootstrapTargetClass);
        trElements.push(testRow);
    }
    
    return trElements;
}


function createUniqueId() {
    return crypto.randomUUID();
}


function createTestclassRow(label, testClassResults, bootstrapTargetClass) {
    const testclassRowElement = document.createElement("tr");
    testclassRowElement.setAttribute("class", "testclass-row");
    testclassRowElement.setAttribute("data-bs-toggle", "collapse");
    testclassRowElement.setAttribute("data-bs-target", "." + bootstrapTargetClass);
    testclassRowElement.setAttribute("aria-expanded", false);

    const testClassNameElement = document.createElement("td");
    testClassNameElement.textContent = makeNameLessTechnical(label);
    testclassRowElement.appendChild(testClassNameElement);

    const totalAmountOfTestsuites = Object.values(testClassResults)[0].length;
    for (let indexOfTestsuite = 0; indexOfTestsuite < totalAmountOfTestsuites; indexOfTestsuite++) {
        testclassRowElement.appendChild(createPassRateElement(testClassResults, indexOfTestsuite));
    }

    return testclassRowElement;
}


function createPassRateElement(testClassResults, indexOfTestsuite) {
    const numberOfPassedTests = getNumberOfPassedTests(testClassResults, indexOfTestsuite);
    const totalAmountOfTests = Object.keys(testClassResults).length;

    let fieldForNumberOfPassedTests = document.createElement("td");
    fieldForNumberOfPassedTests.textContent = numberOfPassedTests + " / " + totalAmountOfTests;
    if (numberOfPassedTests == totalAmountOfTests) {
        fieldForNumberOfPassedTests.style.color = "green";
    } else {
        fieldForNumberOfPassedTests.style.color = "red";
    }

    return fieldForNumberOfPassedTests;
}


function getNumberOfPassedTests(testClassResults, indexOfTestsuite) {
    let numberOfPassedTests = 0;
    const totalAmountOfTests = Object.keys(testClassResults).length;
    for (let indexOfTestcase = 0; indexOfTestcase < totalAmountOfTests; indexOfTestcase++) {
        if (Object.values(testClassResults)[indexOfTestcase][indexOfTestsuite] == "passed") {
            numberOfPassedTests += 1;
        }
    }
    return numberOfPassedTests;
}


function createTestRow(name, results, bootstrapTargetClass) {
    const testRowElement = document.createElement("tr");
    testRowElement.setAttribute("class", "" + bootstrapTargetClass + " collapse");
    const testNameElement = document.createElement("td");
    const humanReadableTestName = makeNameLessTechnical(name);
    testNameElement.textContent = humanReadableTestName;
    testRowElement.appendChild(testNameElement);

    for (const result of results) {
        const resultElement = document.createElement("td");
        resultElement.appendChild(createResultIcon(result));
        testRowElement.appendChild(resultElement);
    }

    return testRowElement;
}


function createResultIcon(resultString) {
    let resultIcon;
    if (resultString == "missing") {
        const missingIcon = document.createElement("i");
        missingIcon.setAttribute("class", "bi bi-question-square-fill");
        resultIcon = missingIcon;
    } else if (resultString == "failed") {
        const failureIcon = document.createElement("i");
        failureIcon.setAttribute("class", "bi bi-exclamation-triangle-fill");
        resultIcon = failureIcon;
    } else {
        const passedIcon = document.createElement("i");
        passedIcon.setAttribute("class", "bi bi-check-lg");
        resultIcon = passedIcon;
    }
    return resultIcon;
}


function extractTestClassResults(JSONObject, referenceTestClassName, referenceTestNames) {
    const testClassResults = {};

    for (const testname of Object.values(referenceTestNames)) {
        let innerArray = [];
        testClassResults[testname] = innerArray;

        for (let testReportIndex = 0; testReportIndex < JSONObject.length; testReportIndex++) {
            if (JSONObject[testReportIndex].tests[referenceTestClassName] === undefined) {
                innerArray.push("missing");           // test class does not exist
            } else if (JSONObject[testReportIndex].tests[referenceTestClassName][testname] === undefined) {
                innerArray.push("missing");           // test class does not exist
            } else {
                const testElement = JSONObject[testReportIndex].tests[referenceTestClassName][testname];
                if (hasFailureChildNode(testElement)) {
                    innerArray.push("failed");
                } else {
                    innerArray.push("passed");
                }
            }
        }
    }

    return testClassResults;
}


function hasFailureChildNode(xmlElement) {
    for (const child of xmlElement.childNodes) {
        if (child.nodeName == "failure") {
            return true;
        }
    }
    return false;
}


function makeNameLessTechnical(originalName) {
    originalName = originalName.replace(/^test_/, "");
    originalName = originalName.replace(/\.test/g, ": ");
    originalName = originalName.replaceAll('_', ' ');
    originalName = originalName.charAt(0).toUpperCase() + originalName.slice(1);

    if (originalName.includes(":")) {
        const arr = originalName.split(": ");
        arr[1] = arr[1].charAt(1).toUpperCase() + arr[1].slice(2);
        originalName = arr.join(": ");
    }

    const humanReadableName = originalName;
    return humanReadableName;
}
