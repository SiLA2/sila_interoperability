"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

import SiLABinaryTransfer_pb2
import abc
import collections.abc
import grpc
import grpc.aio
import typing

_T = typing.TypeVar("_T")

class _MaybeAsyncIterator(collections.abc.AsyncIterator[_T], collections.abc.Iterator[_T], metaclass=abc.ABCMeta): ...

class _ServicerContext(grpc.ServicerContext, grpc.aio.ServicerContext):  # type: ignore[misc, type-arg]
    ...

class BinaryUploadStub:
    def __init__(self, channel: typing.Union[grpc.Channel, grpc.aio.Channel]) -> None: ...
    CreateBinary: grpc.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.CreateBinaryRequest,
        SiLABinaryTransfer_pb2.CreateBinaryResponse,
    ]

    UploadChunk: grpc.StreamStreamMultiCallable[
        SiLABinaryTransfer_pb2.UploadChunkRequest,
        SiLABinaryTransfer_pb2.UploadChunkResponse,
    ]

    DeleteBinary: grpc.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        SiLABinaryTransfer_pb2.DeleteBinaryResponse,
    ]

class BinaryUploadAsyncStub:
    CreateBinary: grpc.aio.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.CreateBinaryRequest,
        SiLABinaryTransfer_pb2.CreateBinaryResponse,
    ]

    UploadChunk: grpc.aio.StreamStreamMultiCallable[
        SiLABinaryTransfer_pb2.UploadChunkRequest,
        SiLABinaryTransfer_pb2.UploadChunkResponse,
    ]

    DeleteBinary: grpc.aio.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        SiLABinaryTransfer_pb2.DeleteBinaryResponse,
    ]

class BinaryUploadServicer(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def CreateBinary(
        self,
        request: SiLABinaryTransfer_pb2.CreateBinaryRequest,
        context: _ServicerContext,
    ) -> typing.Union[SiLABinaryTransfer_pb2.CreateBinaryResponse, collections.abc.Awaitable[SiLABinaryTransfer_pb2.CreateBinaryResponse]]: ...

    @abc.abstractmethod
    def UploadChunk(
        self,
        request_iterator: _MaybeAsyncIterator[SiLABinaryTransfer_pb2.UploadChunkRequest],
        context: _ServicerContext,
    ) -> typing.Union[collections.abc.Iterator[SiLABinaryTransfer_pb2.UploadChunkResponse], collections.abc.AsyncIterator[SiLABinaryTransfer_pb2.UploadChunkResponse]]: ...

    @abc.abstractmethod
    def DeleteBinary(
        self,
        request: SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        context: _ServicerContext,
    ) -> typing.Union[SiLABinaryTransfer_pb2.DeleteBinaryResponse, collections.abc.Awaitable[SiLABinaryTransfer_pb2.DeleteBinaryResponse]]: ...

def add_BinaryUploadServicer_to_server(servicer: BinaryUploadServicer, server: typing.Union[grpc.Server, grpc.aio.Server]) -> None: ...

class BinaryDownloadStub:
    def __init__(self, channel: typing.Union[grpc.Channel, grpc.aio.Channel]) -> None: ...
    GetBinaryInfo: grpc.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.GetBinaryInfoRequest,
        SiLABinaryTransfer_pb2.GetBinaryInfoResponse,
    ]

    GetChunk: grpc.StreamStreamMultiCallable[
        SiLABinaryTransfer_pb2.GetChunkRequest,
        SiLABinaryTransfer_pb2.GetChunkResponse,
    ]

    DeleteBinary: grpc.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        SiLABinaryTransfer_pb2.DeleteBinaryResponse,
    ]

class BinaryDownloadAsyncStub:
    GetBinaryInfo: grpc.aio.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.GetBinaryInfoRequest,
        SiLABinaryTransfer_pb2.GetBinaryInfoResponse,
    ]

    GetChunk: grpc.aio.StreamStreamMultiCallable[
        SiLABinaryTransfer_pb2.GetChunkRequest,
        SiLABinaryTransfer_pb2.GetChunkResponse,
    ]

    DeleteBinary: grpc.aio.UnaryUnaryMultiCallable[
        SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        SiLABinaryTransfer_pb2.DeleteBinaryResponse,
    ]

class BinaryDownloadServicer(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def GetBinaryInfo(
        self,
        request: SiLABinaryTransfer_pb2.GetBinaryInfoRequest,
        context: _ServicerContext,
    ) -> typing.Union[SiLABinaryTransfer_pb2.GetBinaryInfoResponse, collections.abc.Awaitable[SiLABinaryTransfer_pb2.GetBinaryInfoResponse]]: ...

    @abc.abstractmethod
    def GetChunk(
        self,
        request_iterator: _MaybeAsyncIterator[SiLABinaryTransfer_pb2.GetChunkRequest],
        context: _ServicerContext,
    ) -> typing.Union[collections.abc.Iterator[SiLABinaryTransfer_pb2.GetChunkResponse], collections.abc.AsyncIterator[SiLABinaryTransfer_pb2.GetChunkResponse]]: ...

    @abc.abstractmethod
    def DeleteBinary(
        self,
        request: SiLABinaryTransfer_pb2.DeleteBinaryRequest,
        context: _ServicerContext,
    ) -> typing.Union[SiLABinaryTransfer_pb2.DeleteBinaryResponse, collections.abc.Awaitable[SiLABinaryTransfer_pb2.DeleteBinaryResponse]]: ...

def add_BinaryDownloadServicer_to_server(servicer: BinaryDownloadServicer, server: typing.Union[grpc.Server, grpc.aio.Server]) -> None: ...
