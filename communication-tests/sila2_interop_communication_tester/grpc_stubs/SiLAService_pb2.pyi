"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

from . import SiLAFramework_pb2
import builtins
import collections.abc
import google.protobuf.descriptor
import google.protobuf.internal.containers
import google.protobuf.message
import typing

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor

@typing.final
class GetFeatureDefinition_Parameters(google.protobuf.message.Message):
    """Parameters for GetFeatureDefinition"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    FEATUREIDENTIFIER_FIELD_NUMBER: builtins.int
    @property
    def FeatureIdentifier(self) -> SiLAFramework_pb2.String:
        """The fully qualified Feature identifier for which the Feature definition shall be retrieved."""

    def __init__(
        self,
        *,
        FeatureIdentifier: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["FeatureIdentifier", b"FeatureIdentifier"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["FeatureIdentifier", b"FeatureIdentifier"]) -> None: ...

global___GetFeatureDefinition_Parameters = GetFeatureDefinition_Parameters

@typing.final
class GetFeatureDefinition_Responses(google.protobuf.message.Message):
    """Responses of GetFeatureDefinition"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    FEATUREDEFINITION_FIELD_NUMBER: builtins.int
    @property
    def FeatureDefinition(self) -> SiLAFramework_pb2.String:
        """The Feature definition in XML format (according to the Feature Definition Schema)."""

    def __init__(
        self,
        *,
        FeatureDefinition: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["FeatureDefinition", b"FeatureDefinition"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["FeatureDefinition", b"FeatureDefinition"]) -> None: ...

global___GetFeatureDefinition_Responses = GetFeatureDefinition_Responses

@typing.final
class SetServerName_Parameters(google.protobuf.message.Message):
    """Parameters for SetServerName"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERNAME_FIELD_NUMBER: builtins.int
    @property
    def ServerName(self) -> SiLAFramework_pb2.String:
        """The human readable name to assign to the SiLA Server."""

    def __init__(
        self,
        *,
        ServerName: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerName", b"ServerName"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerName", b"ServerName"]) -> None: ...

global___SetServerName_Parameters = SetServerName_Parameters

@typing.final
class SetServerName_Responses(google.protobuf.message.Message):
    """Responses of SetServerName"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___SetServerName_Responses = SetServerName_Responses

@typing.final
class Get_ServerName_Parameters(google.protobuf.message.Message):
    """Parameters for ServerName"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerName_Parameters = Get_ServerName_Parameters

@typing.final
class Get_ServerName_Responses(google.protobuf.message.Message):
    """Responses of ServerName"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERNAME_FIELD_NUMBER: builtins.int
    @property
    def ServerName(self) -> SiLAFramework_pb2.String:
        """Human readable name of the SiLA Server. The name can be set using the 'Set Server Name' command."""

    def __init__(
        self,
        *,
        ServerName: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerName", b"ServerName"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerName", b"ServerName"]) -> None: ...

global___Get_ServerName_Responses = Get_ServerName_Responses

@typing.final
class Get_ServerType_Parameters(google.protobuf.message.Message):
    """Parameters for ServerType"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerType_Parameters = Get_ServerType_Parameters

@typing.final
class Get_ServerType_Responses(google.protobuf.message.Message):
    """Responses of ServerType"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERTYPE_FIELD_NUMBER: builtins.int
    @property
    def ServerType(self) -> SiLAFramework_pb2.String:
        """The type of this server. It, could be, e.g., in the case of a SiLA Device the model name. It is specified by the implementer of the SiLA Server and MAY not be unique."""

    def __init__(
        self,
        *,
        ServerType: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerType", b"ServerType"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerType", b"ServerType"]) -> None: ...

global___Get_ServerType_Responses = Get_ServerType_Responses

@typing.final
class Get_ServerUUID_Parameters(google.protobuf.message.Message):
    """Parameters for ServerUUID"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerUUID_Parameters = Get_ServerUUID_Parameters

@typing.final
class Get_ServerUUID_Responses(google.protobuf.message.Message):
    """Responses of ServerUUID"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERUUID_FIELD_NUMBER: builtins.int
    @property
    def ServerUUID(self) -> SiLAFramework_pb2.String:
        """Globally unique identifier that identifies a SiLA Server. The Server UUID MUST be generated once and remain the same for all times."""

    def __init__(
        self,
        *,
        ServerUUID: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerUUID", b"ServerUUID"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerUUID", b"ServerUUID"]) -> None: ...

global___Get_ServerUUID_Responses = Get_ServerUUID_Responses

@typing.final
class Get_ServerDescription_Parameters(google.protobuf.message.Message):
    """Parameters for ServerDescription"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerDescription_Parameters = Get_ServerDescription_Parameters

@typing.final
class Get_ServerDescription_Responses(google.protobuf.message.Message):
    """Responses of ServerDescription"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERDESCRIPTION_FIELD_NUMBER: builtins.int
    @property
    def ServerDescription(self) -> SiLAFramework_pb2.String:
        """Description of the SiLA Server. This should include the use and purpose of this SiLA Server."""

    def __init__(
        self,
        *,
        ServerDescription: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerDescription", b"ServerDescription"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerDescription", b"ServerDescription"]) -> None: ...

global___Get_ServerDescription_Responses = Get_ServerDescription_Responses

@typing.final
class Get_ServerVersion_Parameters(google.protobuf.message.Message):
    """Parameters for ServerVersion"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerVersion_Parameters = Get_ServerVersion_Parameters

@typing.final
class Get_ServerVersion_Responses(google.protobuf.message.Message):
    """Responses of ServerVersion"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERVERSION_FIELD_NUMBER: builtins.int
    @property
    def ServerVersion(self) -> SiLAFramework_pb2.String:
        """Returns the version of the SiLA Server. A "Major" and a "Minor" version number (e.g. 1.0) MUST be provided, a Patch version number MAY be provided. Optionally, an arbitrary text, separated by an underscore MAY be appended, e.g. “3.19.373_mighty_lab_devices”."""

    def __init__(
        self,
        *,
        ServerVersion: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerVersion", b"ServerVersion"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerVersion", b"ServerVersion"]) -> None: ...

global___Get_ServerVersion_Responses = Get_ServerVersion_Responses

@typing.final
class Get_ServerVendorURL_Parameters(google.protobuf.message.Message):
    """Parameters for ServerVendorURL"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ServerVendorURL_Parameters = Get_ServerVendorURL_Parameters

@typing.final
class Get_ServerVendorURL_Responses(google.protobuf.message.Message):
    """Responses of ServerVendorURL"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    SERVERVENDORURL_FIELD_NUMBER: builtins.int
    @property
    def ServerVendorURL(self) -> SiLAFramework_pb2.String:
        """Returns the URL to the website of the vendor or the website of the product of this SiLA Server. This URL SHOULD be accessible at all times. The URL is a Uniform Resource Locator as defined in RFC 1738."""

    def __init__(
        self,
        *,
        ServerVendorURL: SiLAFramework_pb2.String | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing.Literal["ServerVendorURL", b"ServerVendorURL"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing.Literal["ServerVendorURL", b"ServerVendorURL"]) -> None: ...

global___Get_ServerVendorURL_Responses = Get_ServerVendorURL_Responses

@typing.final
class Get_ImplementedFeatures_Parameters(google.protobuf.message.Message):
    """Parameters for ImplementedFeatures"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    def __init__(
        self,
    ) -> None: ...

global___Get_ImplementedFeatures_Parameters = Get_ImplementedFeatures_Parameters

@typing.final
class Get_ImplementedFeatures_Responses(google.protobuf.message.Message):
    """Responses of ImplementedFeatures"""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    IMPLEMENTEDFEATURES_FIELD_NUMBER: builtins.int
    @property
    def ImplementedFeatures(self) -> google.protobuf.internal.containers.RepeatedCompositeFieldContainer[SiLAFramework_pb2.String]:
        """Returns a list of fully qualified Feature identifiers of all implemented Features of this SiLA Server. This list SHOULD remain the same throughout the lifetime of the SiLA Server."""

    def __init__(
        self,
        *,
        ImplementedFeatures: collections.abc.Iterable[SiLAFramework_pb2.String] | None = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing.Literal["ImplementedFeatures", b"ImplementedFeatures"]) -> None: ...

global___Get_ImplementedFeatures_Responses = Get_ImplementedFeatures_Responses
