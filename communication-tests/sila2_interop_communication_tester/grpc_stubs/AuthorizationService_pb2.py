# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# NO CHECKED-IN PROTOBUF GENCODE
# source: AuthorizationService.proto
# Protobuf Python Version: 5.27.2
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import runtime_version as _runtime_version
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
_runtime_version.ValidateProtobufRuntimeVersion(
    _runtime_version.Domain.PUBLIC,
    5,
    27,
    2,
    '',
    'AuthorizationService.proto'
)
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from . import SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1a\x41uthorizationService.proto\x12\x33sila2.org.silastandard.core.authorizationservice.v1\x1a\x13SiLAFramework.proto\"2\n0Get_FCPAffectedByMetadata_AccessToken_Parameters\"h\n/Get_FCPAffectedByMetadata_AccessToken_Responses\x12\x35\n\rAffectedCalls\x18\x01 \x03(\x0b\x32\x1e.sila2.org.silastandard.String\"K\n\x14Metadata_AccessToken\x12\x33\n\x0b\x41\x63\x63\x65ssToken\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\x8f\x02\n\x14\x41uthorizationService\x12\xf6\x01\n%Get_FCPAffectedByMetadata_AccessToken\x12\x65.sila2.org.silastandard.core.authorizationservice.v1.Get_FCPAffectedByMetadata_AccessToken_Parameters\x1a\x64.sila2.org.silastandard.core.authorizationservice.v1.Get_FCPAffectedByMetadata_AccessToken_Responses\"\x00\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'AuthorizationService_pb2', _globals)
if not _descriptor._USE_C_DESCRIPTORS:
  DESCRIPTOR._loaded_options = None
  _globals['_GET_FCPAFFECTEDBYMETADATA_ACCESSTOKEN_PARAMETERS']._serialized_start=104
  _globals['_GET_FCPAFFECTEDBYMETADATA_ACCESSTOKEN_PARAMETERS']._serialized_end=154
  _globals['_GET_FCPAFFECTEDBYMETADATA_ACCESSTOKEN_RESPONSES']._serialized_start=156
  _globals['_GET_FCPAFFECTEDBYMETADATA_ACCESSTOKEN_RESPONSES']._serialized_end=260
  _globals['_METADATA_ACCESSTOKEN']._serialized_start=262
  _globals['_METADATA_ACCESSTOKEN']._serialized_end=337
  _globals['_AUTHORIZATIONSERVICE']._serialized_start=340
  _globals['_AUTHORIZATIONSERVICE']._serialized_end=611
# @@protoc_insertion_point(module_scope)
