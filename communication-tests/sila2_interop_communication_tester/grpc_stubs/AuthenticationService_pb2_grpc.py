# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc
import warnings

from . import AuthenticationService_pb2 as AuthenticationService__pb2

GRPC_GENERATED_VERSION = '1.66.2'
GRPC_VERSION = grpc.__version__
_version_not_supported = False

try:
    from grpc._utilities import first_version_is_lower
    _version_not_supported = first_version_is_lower(GRPC_VERSION, GRPC_GENERATED_VERSION)
except ImportError:
    _version_not_supported = True

if _version_not_supported:
    raise RuntimeError(
        f'The grpc package installed is at version {GRPC_VERSION},'
        + f' but the generated code in AuthenticationService_pb2_grpc.py depends on'
        + f' grpcio>={GRPC_GENERATED_VERSION}.'
        + f' Please upgrade your grpc module to grpcio>={GRPC_GENERATED_VERSION}'
        + f' or downgrade your generated code using grpcio-tools<={GRPC_VERSION}.'
    )


class AuthenticationServiceStub(object):
    """This Feature provides SiLA Clients with access tokens based on a user identification and password. 1. the user needs to login with the Login command into the server with a user identification (=user name) and a password 2. after verification, an Access Token with the Token Lifetime information will be generated and provided by the server. 3. the user can log-out from the server with the Logout command - a valid Access Token is required to run this command. 
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Login = channel.unary_unary(
                '/sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService/Login',
                request_serializer=AuthenticationService__pb2.Login_Parameters.SerializeToString,
                response_deserializer=AuthenticationService__pb2.Login_Responses.FromString,
                _registered_method=True)
        self.Logout = channel.unary_unary(
                '/sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService/Logout',
                request_serializer=AuthenticationService__pb2.Logout_Parameters.SerializeToString,
                response_deserializer=AuthenticationService__pb2.Logout_Responses.FromString,
                _registered_method=True)


class AuthenticationServiceServicer(object):
    """This Feature provides SiLA Clients with access tokens based on a user identification and password. 1. the user needs to login with the Login command into the server with a user identification (=user name) and a password 2. after verification, an Access Token with the Token Lifetime information will be generated and provided by the server. 3. the user can log-out from the server with the Logout command - a valid Access Token is required to run this command. 
    """

    def Login(self, request, context):
        """Provides an access token based on user information. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Logout(self, request, context):
        """Invalidates the given access token immediately. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_AuthenticationServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Login': grpc.unary_unary_rpc_method_handler(
                    servicer.Login,
                    request_deserializer=AuthenticationService__pb2.Login_Parameters.FromString,
                    response_serializer=AuthenticationService__pb2.Login_Responses.SerializeToString,
            ),
            'Logout': grpc.unary_unary_rpc_method_handler(
                    servicer.Logout,
                    request_deserializer=AuthenticationService__pb2.Logout_Parameters.FromString,
                    response_serializer=AuthenticationService__pb2.Logout_Responses.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))
    server.add_registered_method_handlers('sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService', rpc_method_handlers)


 # This class is part of an EXPERIMENTAL API.
class AuthenticationService(object):
    """This Feature provides SiLA Clients with access tokens based on a user identification and password. 1. the user needs to login with the Login command into the server with a user identification (=user name) and a password 2. after verification, an Access Token with the Token Lifetime information will be generated and provided by the server. 3. the user can log-out from the server with the Logout command - a valid Access Token is required to run this command. 
    """

    @staticmethod
    def Login(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService/Login',
            AuthenticationService__pb2.Login_Parameters.SerializeToString,
            AuthenticationService__pb2.Login_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def Logout(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/sila2.org.silastandard.core.authenticationservice.v1.AuthenticationService/Logout',
            AuthenticationService__pb2.Logout_Parameters.SerializeToString,
            AuthenticationService__pb2.Logout_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)
