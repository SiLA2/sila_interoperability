"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

from . import AuthorizationService_pb2
import abc
import collections.abc
import grpc
import grpc.aio
import typing

_T = typing.TypeVar("_T")

class _MaybeAsyncIterator(collections.abc.AsyncIterator[_T], collections.abc.Iterator[_T], metaclass=abc.ABCMeta): ...

class _ServicerContext(grpc.ServicerContext, grpc.aio.ServicerContext):  # type: ignore[misc, type-arg]
    ...

class AuthorizationServiceStub:
    """This Feature provides access control for the implementing server. It specifies the SiLA Client Metadata for the access token, that has been provided by the AuthenticationService core Feature."""

    def __init__(self, channel: typing.Union[grpc.Channel, grpc.aio.Channel]) -> None: ...
    Get_FCPAffectedByMetadata_AccessToken: grpc.UnaryUnaryMultiCallable[
        AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Parameters,
        AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Responses,
    ]
    """Get fully qualified identifiers of all features, commands and properties affected by AccessToken"""

class AuthorizationServiceAsyncStub:
    """This Feature provides access control for the implementing server. It specifies the SiLA Client Metadata for the access token, that has been provided by the AuthenticationService core Feature."""

    Get_FCPAffectedByMetadata_AccessToken: grpc.aio.UnaryUnaryMultiCallable[
        AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Parameters,
        AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Responses,
    ]
    """Get fully qualified identifiers of all features, commands and properties affected by AccessToken"""

class AuthorizationServiceServicer(metaclass=abc.ABCMeta):
    """This Feature provides access control for the implementing server. It specifies the SiLA Client Metadata for the access token, that has been provided by the AuthenticationService core Feature."""

    @abc.abstractmethod
    def Get_FCPAffectedByMetadata_AccessToken(
        self,
        request: AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Responses, collections.abc.Awaitable[AuthorizationService_pb2.Get_FCPAffectedByMetadata_AccessToken_Responses]]:
        """Get fully qualified identifiers of all features, commands and properties affected by AccessToken"""

def add_AuthorizationServiceServicer_to_server(servicer: AuthorizationServiceServicer, server: typing.Union[grpc.Server, grpc.aio.Server]) -> None: ...
