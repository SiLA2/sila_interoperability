# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# NO CHECKED-IN PROTOBUF GENCODE
# source: ObservablePropertyTest.proto
# Protobuf Python Version: 5.27.2
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import runtime_version as _runtime_version
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
_runtime_version.ValidateProtobufRuntimeVersion(
    _runtime_version.Domain.PUBLIC,
    5,
    27,
    2,
    '',
    'ObservablePropertyTest.proto'
)
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from . import SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1cObservablePropertyTest.proto\x12\x35sila2.org.silastandard.test.observablepropertytest.v1\x1a\x13SiLAFramework.proto\"E\n\x13SetValue_Parameters\x12.\n\x05Value\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x14\n\x12SetValue_Responses\"!\n\x1fSubscribe_FixedValue_Parameters\"U\n\x1eSubscribe_FixedValue_Responses\x12\x33\n\nFixedValue\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\"\n Subscribe_Alternating_Parameters\"W\n\x1fSubscribe_Alternating_Responses\x12\x34\n\x0b\x41lternating\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Boolean\"\x1f\n\x1dSubscribe_Editable_Parameters\"Q\n\x1cSubscribe_Editable_Responses\x12\x31\n\x08\x45\x64itable\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer2\x9f\x06\n\x16ObservablePropertyTest\x12\xa3\x01\n\x08SetValue\x12J.sila2.org.silastandard.test.observablepropertytest.v1.SetValue_Parameters\x1aI.sila2.org.silastandard.test.observablepropertytest.v1.SetValue_Responses\"\x00\x12\xc9\x01\n\x14Subscribe_FixedValue\x12V.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_FixedValue_Parameters\x1aU.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_FixedValue_Responses\"\x00\x30\x01\x12\xcc\x01\n\x15Subscribe_Alternating\x12W.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_Alternating_Parameters\x1aV.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_Alternating_Responses\"\x00\x30\x01\x12\xc3\x01\n\x12Subscribe_Editable\x12T.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_Editable_Parameters\x1aS.sila2.org.silastandard.test.observablepropertytest.v1.Subscribe_Editable_Responses\"\x00\x30\x01\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'ObservablePropertyTest_pb2', _globals)
if not _descriptor._USE_C_DESCRIPTORS:
  DESCRIPTOR._loaded_options = None
  _globals['_SETVALUE_PARAMETERS']._serialized_start=108
  _globals['_SETVALUE_PARAMETERS']._serialized_end=177
  _globals['_SETVALUE_RESPONSES']._serialized_start=179
  _globals['_SETVALUE_RESPONSES']._serialized_end=199
  _globals['_SUBSCRIBE_FIXEDVALUE_PARAMETERS']._serialized_start=201
  _globals['_SUBSCRIBE_FIXEDVALUE_PARAMETERS']._serialized_end=234
  _globals['_SUBSCRIBE_FIXEDVALUE_RESPONSES']._serialized_start=236
  _globals['_SUBSCRIBE_FIXEDVALUE_RESPONSES']._serialized_end=321
  _globals['_SUBSCRIBE_ALTERNATING_PARAMETERS']._serialized_start=323
  _globals['_SUBSCRIBE_ALTERNATING_PARAMETERS']._serialized_end=357
  _globals['_SUBSCRIBE_ALTERNATING_RESPONSES']._serialized_start=359
  _globals['_SUBSCRIBE_ALTERNATING_RESPONSES']._serialized_end=446
  _globals['_SUBSCRIBE_EDITABLE_PARAMETERS']._serialized_start=448
  _globals['_SUBSCRIBE_EDITABLE_PARAMETERS']._serialized_end=479
  _globals['_SUBSCRIBE_EDITABLE_RESPONSES']._serialized_start=481
  _globals['_SUBSCRIBE_EDITABLE_RESPONSES']._serialized_end=562
  _globals['_OBSERVABLEPROPERTYTEST']._serialized_start=565
  _globals['_OBSERVABLEPROPERTYTEST']._serialized_end=1364
# @@protoc_insertion_point(module_scope)
