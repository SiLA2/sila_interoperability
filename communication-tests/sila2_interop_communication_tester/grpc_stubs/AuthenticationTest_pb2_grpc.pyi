"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

from . import AuthenticationTest_pb2
import abc
import collections.abc
import grpc
import grpc.aio
import typing

_T = typing.TypeVar("_T")

class _MaybeAsyncIterator(collections.abc.AsyncIterator[_T], collections.abc.Iterator[_T], metaclass=abc.ABCMeta): ...

class _ServicerContext(grpc.ServicerContext, grpc.aio.ServicerContext):  # type: ignore[misc, type-arg]
    ...

class AuthenticationTestStub:
    """Contains commands that require authentication. A client should be able to obtain an Authorization Token through the Login command of the Authentication Service feature using the following credentials: username: 'test', password: 'test'"""

    def __init__(self, channel: typing.Union[grpc.Channel, grpc.aio.Channel]) -> None: ...
    RequiresToken: grpc.UnaryUnaryMultiCallable[
        AuthenticationTest_pb2.RequiresToken_Parameters,
        AuthenticationTest_pb2.RequiresToken_Responses,
    ]
    """Requires an authorization token in order to be executed"""

    RequiresTokenForBinaryUpload: grpc.UnaryUnaryMultiCallable[
        AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Parameters,
        AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Responses,
    ]
    """Requires an authorization token in order to be executed and to upload a binary parameter"""

class AuthenticationTestAsyncStub:
    """Contains commands that require authentication. A client should be able to obtain an Authorization Token through the Login command of the Authentication Service feature using the following credentials: username: 'test', password: 'test'"""

    RequiresToken: grpc.aio.UnaryUnaryMultiCallable[
        AuthenticationTest_pb2.RequiresToken_Parameters,
        AuthenticationTest_pb2.RequiresToken_Responses,
    ]
    """Requires an authorization token in order to be executed"""

    RequiresTokenForBinaryUpload: grpc.aio.UnaryUnaryMultiCallable[
        AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Parameters,
        AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Responses,
    ]
    """Requires an authorization token in order to be executed and to upload a binary parameter"""

class AuthenticationTestServicer(metaclass=abc.ABCMeta):
    """Contains commands that require authentication. A client should be able to obtain an Authorization Token through the Login command of the Authentication Service feature using the following credentials: username: 'test', password: 'test'"""

    @abc.abstractmethod
    def RequiresToken(
        self,
        request: AuthenticationTest_pb2.RequiresToken_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[AuthenticationTest_pb2.RequiresToken_Responses, collections.abc.Awaitable[AuthenticationTest_pb2.RequiresToken_Responses]]:
        """Requires an authorization token in order to be executed"""

    @abc.abstractmethod
    def RequiresTokenForBinaryUpload(
        self,
        request: AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Responses, collections.abc.Awaitable[AuthenticationTest_pb2.RequiresTokenForBinaryUpload_Responses]]:
        """Requires an authorization token in order to be executed and to upload a binary parameter"""

def add_AuthenticationTestServicer_to_server(servicer: AuthenticationTestServicer, server: typing.Union[grpc.Server, grpc.aio.Server]) -> None: ...
