# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc
import warnings

from . import MetadataConsumerTest_pb2 as MetadataConsumerTest__pb2

GRPC_GENERATED_VERSION = '1.66.2'
GRPC_VERSION = grpc.__version__
_version_not_supported = False

try:
    from grpc._utilities import first_version_is_lower
    _version_not_supported = first_version_is_lower(GRPC_VERSION, GRPC_GENERATED_VERSION)
except ImportError:
    _version_not_supported = True

if _version_not_supported:
    raise RuntimeError(
        f'The grpc package installed is at version {GRPC_VERSION},'
        + f' but the generated code in MetadataConsumerTest_pb2_grpc.py depends on'
        + f' grpcio>={GRPC_GENERATED_VERSION}.'
        + f' Please upgrade your grpc module to grpcio>={GRPC_GENERATED_VERSION}'
        + f' or downgrade your generated code using grpcio-tools<={GRPC_VERSION}.'
    )


class MetadataConsumerTestStub(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature. 
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.EchoStringMetadata = channel.unary_unary(
                '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/EchoStringMetadata',
                request_serializer=MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.SerializeToString,
                response_deserializer=MetadataConsumerTest__pb2.EchoStringMetadata_Responses.FromString,
                _registered_method=True)
        self.UnpackMetadata = channel.unary_unary(
                '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/UnpackMetadata',
                request_serializer=MetadataConsumerTest__pb2.UnpackMetadata_Parameters.SerializeToString,
                response_deserializer=MetadataConsumerTest__pb2.UnpackMetadata_Responses.FromString,
                _registered_method=True)
        self.Get_ReceivedStringMetadata = channel.unary_unary(
                '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/Get_ReceivedStringMetadata',
                request_serializer=MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Parameters.SerializeToString,
                response_deserializer=MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Responses.FromString,
                _registered_method=True)
        self.Subscribe_ReceivedStringMetadataAsCharacters = channel.unary_stream(
                '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/Subscribe_ReceivedStringMetadataAsCharacters',
                request_serializer=MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Parameters.SerializeToString,
                response_deserializer=MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Responses.FromString,
                _registered_method=True)


class MetadataConsumerTestServicer(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature. 
    """

    def EchoStringMetadata(self, request, context):
        """Expects the "String Metadata" metadata from the "Metadata Provider" feature and responds with the metadata value. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UnpackMetadata(self, request, context):
        """Expects the "String Metadata" and "Two Integers Metadata" metadata from the "Metadata Provider" feature and responds with all three data items. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Get_ReceivedStringMetadata(self, request, context):
        """Expects the "String Metadata" metadata from the "Metadata Provider" feature and returns the metadata value. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Subscribe_ReceivedStringMetadataAsCharacters(self, request, context):
        """Expects the "String Metadata" metadata from the "Metadata Provider" feature and returns all characters of its string value as separate responses. 
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_MetadataConsumerTestServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'EchoStringMetadata': grpc.unary_unary_rpc_method_handler(
                    servicer.EchoStringMetadata,
                    request_deserializer=MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.FromString,
                    response_serializer=MetadataConsumerTest__pb2.EchoStringMetadata_Responses.SerializeToString,
            ),
            'UnpackMetadata': grpc.unary_unary_rpc_method_handler(
                    servicer.UnpackMetadata,
                    request_deserializer=MetadataConsumerTest__pb2.UnpackMetadata_Parameters.FromString,
                    response_serializer=MetadataConsumerTest__pb2.UnpackMetadata_Responses.SerializeToString,
            ),
            'Get_ReceivedStringMetadata': grpc.unary_unary_rpc_method_handler(
                    servicer.Get_ReceivedStringMetadata,
                    request_deserializer=MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Parameters.FromString,
                    response_serializer=MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Responses.SerializeToString,
            ),
            'Subscribe_ReceivedStringMetadataAsCharacters': grpc.unary_stream_rpc_method_handler(
                    servicer.Subscribe_ReceivedStringMetadataAsCharacters,
                    request_deserializer=MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Parameters.FromString,
                    response_serializer=MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Responses.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))
    server.add_registered_method_handlers('sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest', rpc_method_handlers)


 # This class is part of an EXPERIMENTAL API.
class MetadataConsumerTest(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature. 
    """

    @staticmethod
    def EchoStringMetadata(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/EchoStringMetadata',
            MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.EchoStringMetadata_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def UnpackMetadata(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/UnpackMetadata',
            MetadataConsumerTest__pb2.UnpackMetadata_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.UnpackMetadata_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def Get_ReceivedStringMetadata(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/Get_ReceivedStringMetadata',
            MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.Get_ReceivedStringMetadata_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def Subscribe_ReceivedStringMetadataAsCharacters(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(
            request,
            target,
            '/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/Subscribe_ReceivedStringMetadataAsCharacters',
            MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.Subscribe_ReceivedStringMetadataAsCharacters_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)
