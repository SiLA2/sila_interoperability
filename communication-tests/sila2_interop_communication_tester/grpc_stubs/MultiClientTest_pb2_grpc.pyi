"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""

from . import MultiClientTest_pb2
from . import SiLAFramework_pb2
import abc
import collections.abc
import grpc
import grpc.aio
import typing

_T = typing.TypeVar("_T")

class _MaybeAsyncIterator(collections.abc.AsyncIterator[_T], collections.abc.Iterator[_T], metaclass=abc.ABCMeta): ...

class _ServicerContext(grpc.ServicerContext, grpc.aio.ServicerContext):  # type: ignore[misc, type-arg]
    ...

class MultiClientTestStub:
    """This is a feature to test different server behaviors when multiple clients request execution of the same command."""

    def __init__(self, channel: typing.Union[grpc.Channel, grpc.aio.Channel]) -> None: ...
    RunInParallel: grpc.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RunInParallel_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Multiple invocations of this command will be running in parallel"""

    RunInParallel_Info: grpc.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RunInParallel"""

    RunInParallel_Result: grpc.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RunInParallel_Responses,
    ]
    """Retrieve result of RunInParallel"""

    RunQueued: grpc.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RunQueued_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Multiple invocations of this command will be queued"""

    RunQueued_Info: grpc.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RunQueued"""

    RunQueued_Result: grpc.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RunQueued_Responses,
    ]
    """Retrieve result of RunQueued"""

    RejectParallelExecution: grpc.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RejectParallelExecution_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Invocations will be rejected, if there is another command instance already running"""

    RejectParallelExecution_Info: grpc.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RejectParallelExecution"""

    RejectParallelExecution_Result: grpc.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RejectParallelExecution_Responses,
    ]
    """Retrieve result of RejectParallelExecution"""

class MultiClientTestAsyncStub:
    """This is a feature to test different server behaviors when multiple clients request execution of the same command."""

    RunInParallel: grpc.aio.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RunInParallel_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Multiple invocations of this command will be running in parallel"""

    RunInParallel_Info: grpc.aio.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RunInParallel"""

    RunInParallel_Result: grpc.aio.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RunInParallel_Responses,
    ]
    """Retrieve result of RunInParallel"""

    RunQueued: grpc.aio.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RunQueued_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Multiple invocations of this command will be queued"""

    RunQueued_Info: grpc.aio.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RunQueued"""

    RunQueued_Result: grpc.aio.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RunQueued_Responses,
    ]
    """Retrieve result of RunQueued"""

    RejectParallelExecution: grpc.aio.UnaryUnaryMultiCallable[
        MultiClientTest_pb2.RejectParallelExecution_Parameters,
        SiLAFramework_pb2.CommandConfirmation,
    ]
    """Invocations will be rejected, if there is another command instance already running"""

    RejectParallelExecution_Info: grpc.aio.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        SiLAFramework_pb2.ExecutionInfo,
    ]
    """Monitor the state of RejectParallelExecution"""

    RejectParallelExecution_Result: grpc.aio.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID,
        MultiClientTest_pb2.RejectParallelExecution_Responses,
    ]
    """Retrieve result of RejectParallelExecution"""

class MultiClientTestServicer(metaclass=abc.ABCMeta):
    """This is a feature to test different server behaviors when multiple clients request execution of the same command."""

    @abc.abstractmethod
    def RunInParallel(
        self,
        request: MultiClientTest_pb2.RunInParallel_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[SiLAFramework_pb2.CommandConfirmation, collections.abc.Awaitable[SiLAFramework_pb2.CommandConfirmation]]:
        """Multiple invocations of this command will be running in parallel"""

    @abc.abstractmethod
    def RunInParallel_Info(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[collections.abc.Iterator[SiLAFramework_pb2.ExecutionInfo], collections.abc.AsyncIterator[SiLAFramework_pb2.ExecutionInfo]]:
        """Monitor the state of RunInParallel"""

    @abc.abstractmethod
    def RunInParallel_Result(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[MultiClientTest_pb2.RunInParallel_Responses, collections.abc.Awaitable[MultiClientTest_pb2.RunInParallel_Responses]]:
        """Retrieve result of RunInParallel"""

    @abc.abstractmethod
    def RunQueued(
        self,
        request: MultiClientTest_pb2.RunQueued_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[SiLAFramework_pb2.CommandConfirmation, collections.abc.Awaitable[SiLAFramework_pb2.CommandConfirmation]]:
        """Multiple invocations of this command will be queued"""

    @abc.abstractmethod
    def RunQueued_Info(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[collections.abc.Iterator[SiLAFramework_pb2.ExecutionInfo], collections.abc.AsyncIterator[SiLAFramework_pb2.ExecutionInfo]]:
        """Monitor the state of RunQueued"""

    @abc.abstractmethod
    def RunQueued_Result(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[MultiClientTest_pb2.RunQueued_Responses, collections.abc.Awaitable[MultiClientTest_pb2.RunQueued_Responses]]:
        """Retrieve result of RunQueued"""

    @abc.abstractmethod
    def RejectParallelExecution(
        self,
        request: MultiClientTest_pb2.RejectParallelExecution_Parameters,
        context: _ServicerContext,
    ) -> typing.Union[SiLAFramework_pb2.CommandConfirmation, collections.abc.Awaitable[SiLAFramework_pb2.CommandConfirmation]]:
        """Invocations will be rejected, if there is another command instance already running"""

    @abc.abstractmethod
    def RejectParallelExecution_Info(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[collections.abc.Iterator[SiLAFramework_pb2.ExecutionInfo], collections.abc.AsyncIterator[SiLAFramework_pb2.ExecutionInfo]]:
        """Monitor the state of RejectParallelExecution"""

    @abc.abstractmethod
    def RejectParallelExecution_Result(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: _ServicerContext,
    ) -> typing.Union[MultiClientTest_pb2.RejectParallelExecution_Responses, collections.abc.Awaitable[MultiClientTest_pb2.RejectParallelExecution_Responses]]:
        """Retrieve result of RejectParallelExecution"""

def add_MultiClientTestServicer_to_server(servicer: MultiClientTestServicer, server: typing.Union[grpc.Server, grpc.aio.Server]) -> None: ...
